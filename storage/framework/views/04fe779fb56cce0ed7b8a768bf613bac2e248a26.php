<?php $__env->startSection('title','Create user'); ?>
<?php $__env->startSection('content'); ?>

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Data user</div>

                <div class="card-body">

                    <?php echo e(session('status')); ?>

                    

                        <table class="table table-bordered">
                            <tr>
                                <th>NAME</th>
                                <th>EMAIL</th>
                                <th colspan="2"></th>
                            </tr>
                            <?php $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <tr>
                                <td><?php echo e($row->name); ?></td>
                                <td><?php echo e($row->email); ?></td>
                                <td>
                                <?php echo e(link_to('user/'.$row->id.'/edit','Edit',['class'=>'btn btn-info'])); ?>

                                </td>
                                <td>
                                <?php echo e(Form::open(['url'=>'user/'.$row->id,'method'=>'delete'])); ?>

                                
                                <button type="submit" onClick="return confirm('Are You Sure you want to Remove?')">Delete</button>
                                <?php echo e(Form::close()); ?>

                                </td>
                            </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </table>
                    <?php echo e($users->Links()); ?>

                    <?php echo e(link_to('user/create','Create new')); ?>

                </div>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>