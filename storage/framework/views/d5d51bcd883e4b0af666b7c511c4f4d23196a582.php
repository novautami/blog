<table class="table table-bordered">
        <tr>
            <td>NAME</td>
            <td><?php echo e(Form::text('name',null,['placeholder'=>'NAME'])); ?></td>
        </tr>
        <tr>
            <td>EMAIL</td>
            <td><?php echo e(Form::text('email',null,['placeholder'=>'EMAIL'])); ?></td>
        </tr>
        <tr>
            <td>PASSWORD</td>
            <td><?php echo e(Form::password('password',['placeholder'=>'PASSWORD'])); ?></td>
        </tr>
        <tr>
            <td></td>
            <td><?php echo e(Form::submit('Save Data',['class'=>'btn btn-succes'])); ?>

                <?php echo e(Link_to('user','Back',['class'=>'btn btn-warning'])); ?> 
            </td>
        </tr>
</table>