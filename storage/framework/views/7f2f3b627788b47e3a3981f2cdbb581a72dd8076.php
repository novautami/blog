<?php $__env->startSection('title','Create Departemen'); ?>
<?php $__env->startSection('content'); ?>


    <h3>Form Create Departemen</h3>
    <?php if($errors->any()): ?>
    <div class="alert alert-danger">
        <ul>
            <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <li><?php echo e($error); ?></li>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </ul>
    </div>
    <?php endif; ?>

    <?php echo e(Form::model($departemen,['url'=>'departemen/'.$departemen->id,'method'=>'PUT'])); ?>

    <table>
        <tr>
            <td>ID</td>
            <td><?php echo e(Form::text('id',null,['placeholder'=>'ID'])); ?></td>
        </tr>
        <tr>
            <td>DEPARTEMEN</td>
            <td><?php echo e(Form::text('departemen',null,['placeholder'=>'DEPARTEMEN'])); ?></td>
        </tr>
        
        <tr>
            <td></td>
            <td><?php echo e(Form::submit('Save Data')); ?>

                <?php echo e(Link_to('departemen','Back')); ?> 
            </td>
        </tr>
    </table>
    <?php echo e(Form::close()); ?>

<?php $__env->stopSection(); ?> 
<?php echo $__env->make('template', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>