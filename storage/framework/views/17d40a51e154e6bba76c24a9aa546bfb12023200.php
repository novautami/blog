
<!DOCTYPE html>
<html>
<head>
        <title>Daftar Artikel</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
</head>
<body>
<div class="container">
    
    <div class="row">
    
        <div class="col-md-12" style="margin-bottom:12px;">
            <div class="row">
                <div class="col-md-2>">
                    <img src="<?php echo e(asset('image/tol.jpg')); ?>"width="170">
                </div>
                <div class="col-md-10>">
                    <div class="row">
                        <div class="col-md-12">
                            <a href="/article/1"> <?php echo e($article['title']); ?> </a>
                        </div>
                        <div class="col-md-12">
                            <?php echo e($article['publish_date']); ?>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
   <h2>Komentar dari Netizen</h2>
   <div class="row">
        <div class="col-md-2">
            senin,2014-01-01
        </div>
        <div class="col-md-9">
            Artikelnya sangat bermanfaat
        </div>
    </div>
    <h3> Kirim komentar<h3>
        <div class="row">
            <div class="col-md-6">
                <?php echo e(Form::text('name',null,['class'=>'form-control','placeholder'=>'your name'])); ?>

            </div>
            <div class="col-md-6">  
                <?php echo e(Form::text('email',null,['class'=>'form-control','placeholder'=>'your email'])); ?> 
            </div>
            <div class="col-md-12">  
                <?php echo e(Form::textarea('comment',null,['class'=>'form-control','placeholder'=>'your comment'])); ?> 
            </div>
            <div class="col-md-12">  
                <?php echo e(Form::submit('send comment',['class'=>'btn btn-danger'])); ?> 
            </div>
        </div>
</div>
</body>
</html>