<?php $__env->startSection('title','Create Employee'); ?>
<?php $__env->startSection('content'); ?>

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Data Employee</div>

                <div class="card-body">

                    <?php echo e(session('status')); ?>

                    <a href="/employee/excel" class="btn btn-success">Export Excel</a>
                    <a href="/employee/pdf" class="btn btn-danger">Export PDF</a>
                    <hr>
                        <?php echo e(Form::open(['url'=>'employee','method'=>'GET'])); ?>

                        <table class="table table-bordered">
                            <tr><td>Departemen</td>
                                <td>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <?php echo e(Form::select('departemen',$departemens, $parameter,['class'=>'form-control','placeholder'=>'All Data'])); ?>

                                        </div> 
                                        <div class="col-md-6">
                                            <?php echo e(Form::submit('Filter Data',['class'=>'btn btn-info'])); ?>

                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </table>
                        <?php echo e(Form::close()); ?>

                            <table class="table table-bordered">
                            </tr>
                                <th>NO</th>
                                <th>NIK</th>
                                <th>NAME</th>
                                <th>DEPARTEMEN</th>
                                <th colspan="2"></th>
                            </tr>
                            <?php $__currentLoopData = $employees; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <tr>
                                <td><?php echo e($loop->iteration); ?></td>
                                <td><?php echo e($row->nik); ?></td>
                                <td><?php echo e($row->name); ?></td>
                                <td><?php echo e($row->departemen->departemen); ?></td>
                                <td>
                                <?php echo e(link_to('employee/'.$row->nik.'/edit','Edit',['class'=>'btn btn-info'])); ?>

                                </td>
                                <td>
                                <?php echo e(Form::open(['url'=>'employee/'.$row->nik,'method'=>'delete'])); ?>

                                <?php echo e(Form::submit('delete',['class'=>'btn btn-warning'])); ?>

                                <?php echo e(Form::close()); ?>

                                </td>
                            </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </table>
                    <?php echo e($employees->Links()); ?>

                    <?php echo e(link_to('employee/create','Create new')); ?>

                </div>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>