<?php $__env->startSection('title','Create Departemen'); ?>
<?php $__env->startSection('content'); ?>

<?php echo e(session('status')); ?>


    <table border="1">
        <tr>
            <th>ID</th>
            <th>DEPARTEMEN</th>
        </tr>
        <?php $__currentLoopData = $departemens; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <tr>
            <td><?php echo e($row->id); ?></td>
            <td><?php echo e($row->departemen); ?></td>
            <td>
                <?php echo e(link_to('departemen/'.$row->id.'/edit','Edit')); ?>

            </td>
            <td>
                <?php echo e(Form::open(['url'=>'departemen/'.$row->id,'method'=>'delete'])); ?>

                    <?php echo e(Form::submit('delete')); ?>

                <?php echo e(Form::close()); ?>

            </td>
        </tr>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </table>
    <?php echo e(link_to('departemen/create','Create new')); ?>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('template', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>