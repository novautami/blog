<?php $__env->startSection('title','Create Employee'); ?>
<?php $__env->startSection('content'); ?>


<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Create Data Employee</div>

                <div class="card-body">
                    <h3>Form Create Employee</h3>
                    <?php echo e(Form::open(['url'=>'employee'])); ?>

                        <?php echo $__env->make('employee.form', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                    <?php echo e(Form::close()); ?>

                </div>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?> 
        

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>