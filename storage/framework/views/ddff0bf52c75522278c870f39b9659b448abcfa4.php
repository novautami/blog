
<!DOCTYPE html>
<html>
<head>
        <title>Daftar Artikel</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
</head>
<body>
<div class="container">
        <div class="row">
        <?php $__currentLoopData = $articles; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $article): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <div class="col-md-12" style="margin-bottom:12px;">
            <div class="row">
                <div class="col-md-2>">
                <img src="<?php echo e(asset ('image/tol.jpg')); ?>"width="170">
                </div>
                <div class="col-md-10>">
                    <div class="row">
                        <div class="col-md-12">
                            <a href="/article/1"> <?php echo e($article['title']); ?> </a>
                        </div>
                        <div class="col-md-12">
                            <?php echo e($article['publish_date']); ?>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </div>
    </div>
</body>
</html>